﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.EntityState
{
	public enum EntityState
	{
		Detached = 1,
		Unchanged = 2,
		Added = 4,
		Deleted = 8,
		Modified = 16,
	}
}
