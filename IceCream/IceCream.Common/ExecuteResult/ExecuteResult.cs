﻿using IceCream.Common.ExecuteResult.Interface;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.ExecuteResult
{
	public class ExecuteResult : IExecuteResult
	{
		public bool Success { get; set; }
		public object Result { get; set; }

		public string ErrorMessage { get; set; }
	}
}
