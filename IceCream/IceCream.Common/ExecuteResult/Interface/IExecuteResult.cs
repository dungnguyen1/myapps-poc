﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.ExecuteResult.Interface
{
	public interface IExecuteResult
	{
		bool Success { get; set; }
		object Result { get; set; }
		string ErrorMessage { get; set; }
	}
}
