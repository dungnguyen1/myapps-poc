﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.DTO
{
	public class OrderDTO
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Contact { get; set; }
		public string Address { get; set; }
		public double BookCost { get; set; }
		public string PayingOption { get; set; }
		public DateTime OrderDate { get; set; }
		public bool Status { get; set; }
	}
}
