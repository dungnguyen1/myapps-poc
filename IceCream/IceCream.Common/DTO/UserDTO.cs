﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.DTO
{
	public class UserDTO
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public string OldPassword { get; set; }
	}
}
