﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.DTO
{
	public class RecipesDTO
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public string Description { get; set; }
		public string Details { get; set; }
		public string Author { get; set; }
		public int ViewNumber { get; set; }
		public DateTime UploadDate { get; set; }
		public bool EnableStatus { get; set; }
	}
}
