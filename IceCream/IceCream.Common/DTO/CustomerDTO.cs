﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Common.DTO
{
	public class CustomerDTO
	{
		public int ID { get; set; }
		public string UserName { get; set; }
		public string FullName { get; set; }
		public string Email { get; set; }
		public DateTime ExpireDate { get; set; }
		public bool Enabled { get; set; }
		public string Address { get; set; }
		public string PhoneNumber { get; set; }
		public bool? Gender { get; set; }
		public DateTime? Birthday { get; set; }
	}
}
