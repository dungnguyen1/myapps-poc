﻿using IceCream.Common.DTO;
using IceCream.Data;
using IceCream.Data.uow;
using IceCream.Data.uow.Implement;
using IceCream.Service.IServices;
using System.Collections.Generic;
using System.Linq;

namespace IceCream.Service.Services
{
	public class OrderService : IOrderService
	{
		public IMyUow MyUow;

		public OrderService(IMyDbContext _myDbContext)
		{
			MyUow = new MyUow(_myDbContext);
		}

		public List<OrderDTO> GetOnlineOrder()
		{
			var orders = MyUow.OrdeRepository.GetAll().Select(x => new OrderDTO
			{
				ID = x.OrderId,
				Name = x.Name,
				Email = x.Email,
				Address = x.Address,
				BookCost = x.BookCost,
				Contact = x.Contact,
				OrderDate = x.OrderDate,
				PayingOption = x.PayingOption,
				Status = x.Status
			}).OrderByDescending(x => x.ID).ToList();

			return orders;
		}

		public OrderDTO GetOrderById(int id)
		{
			var order = MyUow.OrdeRepository.GetAll().FirstOrDefault(x => x.OrderId == id);
			if (order != null)
			{
				return new OrderDTO
				{
					ID = order.OrderId,
					Name = order.Name,
					Email = order.Email,
					Address = order.Address,
					BookCost = order.BookCost,
					Contact = order.Contact,
					Status = order.Status,
					OrderDate = order.OrderDate,
					PayingOption = order.PayingOption
				};
			}

			return null;
		}

		public OrderDTO UpdateOrderStatus(OrderDTO orderdto)
		{
			var order = MyUow.OrdeRepository.GetAll().FirstOrDefault(x => x.OrderId == orderdto.ID);

			if (order != null)
			{
				order.Status = orderdto.Status;

				MyUow.OrdeRepository.Update(order);
				return MyUow.Commit() > 0 ? orderdto : null;
			}

			return null;
		}
	}
}
