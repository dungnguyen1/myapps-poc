﻿using IceCream.Common.DTO;
using IceCream.Data;
using IceCream.Data.uow;
using IceCream.Data.uow.Implement;
using IceCream.Service.IServices;
using System.Collections.Generic;
using System.Linq;

namespace IceCream.Service.Services
{
	public class CustomerService : ICustomerService
	{
		public IMyUow MyUow;

		public CustomerService(IMyDbContext _myDbContext)
		{
			MyUow = new MyUow(_myDbContext);
		}

		public List<CustomerDTO> GetAllCustomer()
		{
			var customers = MyUow.CustomerRepository.GetAll();
			var result = customers.Select(x => new CustomerDTO
			{
				Email = x.Email,
				Enabled = x.EnableStatus,
				ExpireDate = x.ExpiredDate,
				FullName = x.FullName,
				ID = x.CustomerId,
				UserName = x.Username
			}).ToList();

			return result;
		}

		public int EnableCustomer(List<CustomerDTO> customers)
		{
			List<int> customerIds = customers.Select(x => x.ID).ToList();

			if (customerIds.Any())
			{
				var customerEntitys = MyUow.CustomerRepository.GetAll().Where(x => customerIds.Contains(x.CustomerId)).ToList();

				if (customerEntitys.Any())
				{
					foreach (var entity in customerEntitys)
					{
						entity.EnableStatus = customers.FirstOrDefault(x => x.ID == entity.CustomerId).Enabled;
						MyUow.CustomerRepository.Update(entity);
					}

					return MyUow.Commit();
				}
			}

			return 0;
		}

		public CustomerDTO GetCustomerById(int id)
		{
			var customer = MyUow.CustomerRepository.GetAll().FirstOrDefault(x => x.CustomerId == id);
			if (customer != null)
			{
				return new CustomerDTO
				{
					ID = customer.CustomerId,
					UserName = customer.Username,
					FullName = customer.FullName,
					Address = customer.Address,
					Birthday = customer.Birthday,
					Email = customer.Email,
					Enabled = customer.EnableStatus,
					ExpireDate = customer.ExpiredDate,
					Gender = customer.Gender,
					PhoneNumber = customer.PhoneNumber
				};
			}

			return null;
		}

		public CustomerDTO UpdateCustomerDetail(CustomerDTO customer)
		{
			var customerEntity = MyUow.CustomerRepository.GetAll().FirstOrDefault(x => x.CustomerId == customer.ID);
			if (customerEntity != null)
			{
				customerEntity.ExpiredDate = customer.ExpireDate.ToLocalTime();
				customerEntity.EnableStatus = customer.Enabled;

				MyUow.CustomerRepository.Update(customerEntity);

				var result = MyUow.Commit();
				if (result > 0) return customer;
			}

			return null;
		}
	}
}
