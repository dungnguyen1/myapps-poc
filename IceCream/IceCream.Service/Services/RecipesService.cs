﻿using IceCream.Common.DTO;
using IceCream.Data;
using IceCream.Data.uow;
using IceCream.Data.uow.Implement;
using IceCream.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IceCream.Service.Services
{
	public class RecipesService : IRecipesService
	{
		public IMyUow MyUow;

		public RecipesService(IMyDbContext _myDbContext)
		{
			MyUow = new MyUow(_myDbContext);
		}

		public List<RecipesDTO> GetAllRecipes()
		{
			var result = MyUow.RecipesRepository.GetAll().Select(x => new RecipesDTO
			{
				ID = x.RecipeId,
				EnableStatus = x.EnableStatus,
				Author = x.Author,
				Description = x.Desciption,
				Details = x.Details,
				Image = x.Image,
				Name = x.Name,
				UploadDate = x.UploadDate,
				ViewNumber = x.ViewNumber
			}).ToList();

			return result;
		}

		public RecipesDTO GetRecipeById(int id)
		{
			var recipe = MyUow.RecipesRepository.GetAll().FirstOrDefault(x => x.RecipeId == id);

			if (recipe != null)
			{
				return new RecipesDTO
				{
					ID = recipe.RecipeId,
					EnableStatus = recipe.EnableStatus,
					Author = recipe.Author,
					Description = recipe.Desciption,
					Details = recipe.Details,
					Image = recipe.Image,
					Name = recipe.Name,
					UploadDate = recipe.UploadDate,
					ViewNumber = recipe.ViewNumber
				};
			}

			return null;
		}

		public RecipesDTO UpdateRecipe(RecipesDTO recipe)
		{
			var data = MyUow.RecipesRepository.GetAll().FirstOrDefault(x => x.RecipeId == recipe.ID);
			if (data != null)
			{
				data.Name = recipe.Name;
				data.Image = recipe.Image;
				data.Desciption = recipe.Description;
				data.Details = recipe.Details;
				data.Author = recipe.Author;
				data.EnableStatus = recipe.EnableStatus;

				MyUow.RecipesRepository.Update(data);

				var status = MyUow.Commit();
				if (status > 0) return recipe;
				else return null;
			}

			return null;
		}

		public RecipesDTO AddRecipe(RecipesDTO recipe)
		{
			var recipeEntity = new Recipe
			{
				Name = recipe.Name,
				Desciption = recipe.Description,
				Details = recipe.Details,
				Author = recipe.Author,
				Image = recipe.Image,
				EnableStatus = recipe.EnableStatus,
				UploadDate = recipe.UploadDate
			};

			MyUow.RecipesRepository.Add(recipeEntity);

			var result = MyUow.Commit();
			if (result > 0) return recipe;
			else return null;
		}
	}
}
