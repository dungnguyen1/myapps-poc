﻿using IceCream.Common.DTO;
using IceCream.Data;
using IceCream.Data.uow;
using IceCream.Data.uow.Implement;
using IceCream.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IceCream.Service.Services
{
	public class AccountService : IAccountService
	{
		public IMyUow MyUow;

		public AccountService(IMyDbContext _myDbContext)
		{
			MyUow = new MyUow(_myDbContext);
		}

		public bool CheckUserNameAndPass(string user, string pass)
		{
			var result = MyUow.AdminRepository.GetAll()
				.FirstOrDefault(x => x.Username == user.ToLower().Trim() && x.Password == pass.ToLower().Trim());

			if (result != null) return true;
			return false;
		}

		public UserDTO CheckUserNameAndPassReturnUser(string user, string pass)
		{
			var result = MyUow.AdminRepository.GetAll()
				.FirstOrDefault(x => x.Username == user.ToLower().Trim() && x.Password == pass.ToLower().Trim());

			if (result != null) return new UserDTO
			{
				Username = result.Username
			};

			return null;
		}

		public UserDTO ChangePassword(string userName, string newPass, string oldPass)
		{
			var user = MyUow.AdminRepository.GetAll().FirstOrDefault(x => x.Username == userName);
			if (user.Password != oldPass) return null;

			if (user != null)
			{
				user.Password = newPass;

				MyUow.AdminRepository.Update(user);
				var result = MyUow.Commit();

				return result > 0 ? new UserDTO
				{
					Username = userName
				} : null;
			}

			return null;
		}
	}
}
