﻿using IceCream.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Service.IServices
{
	public interface ICustomerService
	{
		List<CustomerDTO> GetAllCustomer();
		int EnableCustomer(List<CustomerDTO> customers);
		CustomerDTO GetCustomerById(int id);
		CustomerDTO UpdateCustomerDetail(CustomerDTO customer);
	}
}
