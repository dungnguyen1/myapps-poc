﻿using IceCream.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IceCream.Service.IServices
{
	public interface IAccountService
	{
		bool CheckUserNameAndPass(string user, string pass);
		UserDTO CheckUserNameAndPassReturnUser(string user, string pass);
		UserDTO ChangePassword(string userName, string newPass, string oldPass);
	}
}
