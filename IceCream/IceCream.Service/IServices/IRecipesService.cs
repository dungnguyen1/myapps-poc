﻿using IceCream.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Service.IServices
{
	public interface IRecipesService
	{
		List<RecipesDTO> GetAllRecipes();
		RecipesDTO GetRecipeById(int id);
		RecipesDTO UpdateRecipe(RecipesDTO recipe);
		RecipesDTO AddRecipe(RecipesDTO recipe);
	}
}
