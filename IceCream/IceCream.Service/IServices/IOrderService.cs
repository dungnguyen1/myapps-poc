﻿using IceCream.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Service.IServices
{
	public interface IOrderService
	{
		List<OrderDTO> GetOnlineOrder();
		OrderDTO GetOrderById(int id);
		OrderDTO UpdateOrderStatus(OrderDTO orderdto);
	}
}
