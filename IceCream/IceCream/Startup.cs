using System;
using System.Text;
using IceCream.Data;
using IceCream.Provider.TokenProvider;
using IceCream.Service.IServices;
using IceCream.Service.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace IceCream
{
	public class Startup
	{
		// secretKey contains a secret passphrase only your server knows
		private string secretKey => Configuration.GetSection("AuthenticationToken:Token").Value;
		private string validAudience => Configuration.GetSection("AuthenticationToken:ValidAudience").Value;
		private string validIssuer => Configuration.GetSection("AuthenticationToken:ValidIssuer").Value;
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();

			services.AddTransient<IMyDbContext, MyDbContext>(_ => new MyDbContext(Configuration.GetConnectionString("MyDbContext")));
			services.AddTransient<ICustomerService, CustomerService>();
			services.AddTransient<IAccountService, AccountService>();
			services.AddTransient<IRecipesService, RecipesService>();
			services.AddTransient<IOrderService, OrderService>();

			services.AddControllers().AddNewtonsoftJson(o => o.SerializerSettings.ContractResolver = new DefaultContractResolver());

			var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
			var tokenValidationParameters = new TokenValidationParameters
			{
				// The signing key must match!
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = signingKey,

				// Validate the JWT Issuer (iss) claim
				ValidateIssuer = true,
				ValidIssuer = validIssuer,

				// Validate the JWT Audience (aud) claim
				ValidateAudience = true,
				ValidAudience = validAudience,

				// Validate the token expiry
				ValidateLifetime = true,

				// If you want to allow a certain amount of clock drift, set that here:
				ClockSkew = TimeSpan.Zero
			};

			//Add cookie authentication
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddCookie(o =>
			{
				o.Cookie.Name = "access_token";
				o.TicketDataFormat = new CustomJwtDataFormat(SecurityAlgorithms.HmacSha256, tokenValidationParameters);
			})
			.AddJwtBearer(x =>
			{
				x.TokenValidationParameters = tokenValidationParameters;
			});

			services.AddMvc().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNameCaseInsensitive = false);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseCors(builder =>
			{
				builder.AllowAnyHeader();
				builder.AllowAnyMethod();
				//builder.AllowCredentials();
				//builder.AllowAnyOrigin(); // For anyone access.
				builder.WithOrigins(Configuration.GetSection("CorsURL:url").Value); // for a specific url.
			});

			app.UseHttpsRedirection();

			app.UseRouting();

			//Add JWT generation endpoint:
			var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
			var options = new TokenProviderOptions
			{
				Audience = validAudience,
				Issuer = validIssuer,
				SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
			};

			app.UseMiddleware<TokenProviderMiddleware>(Options.Create(options));

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
