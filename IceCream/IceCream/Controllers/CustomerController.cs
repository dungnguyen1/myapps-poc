﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCream.Common.DTO;
using IceCream.Common.ExecuteResult;
using IceCream.Common.ExecuteResult.Interface;
using IceCream.Service.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IceCream.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
    public class CustomerController : ControllerBase
    {
		public ICustomerService customerService { get; set; }

		public CustomerController(ICustomerService _customerService)
		{
			customerService = _customerService;
		}

		[Route("GetCustomer")]
		[HttpGet]
		public IExecuteResult GetCustomer()
		{
			var result = customerService.GetAllCustomer();

			return new ExecuteResult
			{
				Success = true,
				Result = result
			};
		}

		[Route("GetCustomerDetail/{Id}")]
		[HttpGet]
		public IExecuteResult GetCustomerById(int Id)
		{
			var result = customerService.GetCustomerById(Id);

			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}
			else
			{
				return new ExecuteResult
				{
					Success = false,
					Result = null
				};
			}

		}

		[Route("EnableCustomer")]
		[HttpPost]
		public IExecuteResult EnableCustomer(List<CustomerDTO> customers)
		{
			var result = customerService.EnableCustomer(customers);

			if (result <= 0)
			{
				return new ExecuteResult
				{
					Success = false,
					ErrorMessage = "Can not update customer. Please contact admin."
				};
			}
			else
			{
				return new ExecuteResult
				{
					Success = true,
					Result = customers
				};
			}
		}

		[HttpPost]
		[Route("UpdateDetail")]
		public IExecuteResult UpdateCustomerDetail(CustomerDTO customer)
		{
			var result = customerService.UpdateCustomerDetail(customer);
			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}
	}
}