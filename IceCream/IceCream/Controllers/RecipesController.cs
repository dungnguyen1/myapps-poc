﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IceCream.Common.DTO;
using IceCream.Common.ExecuteResult;
using IceCream.Common.ExecuteResult.Interface;
using IceCream.Service.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IceCream.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	[Authorize]
    public class RecipesController : ControllerBase
    {
		public IRecipesService recipesService { get; set; }

		public RecipesController(IRecipesService _recipesService)
		{
			recipesService = _recipesService;
		}

		[Route("GetAllRecipes")]
		[HttpGet]
		public IExecuteResult GetAllRecipes()
		{
			var result = recipesService.GetAllRecipes();

			return new ExecuteResult
			{
				Success = true,
				Result = result
			};
		}

		[Route("GetRecipeById/{id}")]
		[HttpGet]
		public IExecuteResult GetRecipeById(int id)
		{
			var result = recipesService.GetRecipeById(id);

			return new ExecuteResult
			{
				Success = true,
				Result = result
			};
		}

		[Route("UpdateRecipe")]
		[HttpPost]
		public IExecuteResult UpdateRecipe(RecipesDTO recipe)
		{
			var result = recipesService.UpdateRecipe(recipe);

			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}

		[Route("AddRecipe")]
		[HttpPost]
		public IExecuteResult AddRecipe(RecipesDTO recipe)
		{
			var result = recipesService.AddRecipe(recipe);

			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}
	}
}