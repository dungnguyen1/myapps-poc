﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IceCream.Common.DTO;
using IceCream.Common.ExecuteResult;
using IceCream.Common.ExecuteResult.Interface;
using IceCream.Service.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IceCream.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
		private IAccountService accountService { get; set; }

		public AccountController(IAccountService _accountService)
		{
			accountService = _accountService;
		}

		[Route("GetClaimUser")]
		[HttpGet]
		public IExecuteResult GetClaimsUser()
		{
			//var identityClaims = (ClaimsIdentity)User.Identity;
			//IEnumerable<Claim> claims = identityClaims.Claims;
			UserDTO model = new UserDTO()
			{
				Username = "Test"
			};
			return new ExecuteResult
			{
				Success = true,
				Result = model
			};
		}

		[Route("ChangePassword")]
		[HttpPost]
		[Authorize]
		public IExecuteResult ChangePassword(UserDTO user)
		{
			var result = accountService.ChangePassword(user.Username, user.Password, user.OldPassword);

			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}
	}
}
