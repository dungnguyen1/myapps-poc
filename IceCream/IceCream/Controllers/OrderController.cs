﻿using IceCream.Common.DTO;
using IceCream.Common.ExecuteResult;
using IceCream.Common.ExecuteResult.Interface;
using IceCream.Service.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IceCream.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
	[Authorize]
    public class OrderController : ControllerBase
    {
		public IOrderService orderService { get; set; }

		public OrderController(IOrderService _orderService)
		{
			orderService = _orderService;
		}

		[Route("GetAll")]
		[HttpGet]
		public IExecuteResult GetAll()
		{
			var result = orderService.GetOnlineOrder();

			return new ExecuteResult
			{
				Success = true,
				Result = result
			};
		}

		[Route("UpdateOrder")]
		[HttpPost]
		public IExecuteResult UpdateOrder(OrderDTO order)
		{
			var result = orderService.UpdateOrderStatus(order);

			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}

		[Route("GetOrderById/{id}")]
		[HttpGet]
		public IExecuteResult GetOrderById(int id)
		{
			var result = orderService.GetOrderById(id);
			if (result != null)
			{
				return new ExecuteResult
				{
					Success = true,
					Result = result
				};
			}

			return new ExecuteResult
			{
				Success = false
			};
		}
	}
}
