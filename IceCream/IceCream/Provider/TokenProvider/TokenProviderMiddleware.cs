﻿using IceCream.Common.DTO;
using IceCream.Service.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IceCream.Provider.TokenProvider
{
	public class TokenProviderMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly TokenProviderOptions _options;
		private IAccountService accountService;

		public TokenProviderMiddleware(
			RequestDelegate next,
			IOptions<TokenProviderOptions> options, IAccountService _accountService)
		{
			_next = next;
			_options = options.Value;
			accountService = _accountService;
		}

		public Task Invoke(HttpContext context)
		{
			// If the request path doesn't match, skip
			if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
			{
				return _next(context);
			}

			// Request must be POST with Content-Type: application/x-www-form-urlencoded
			if (!context.Request.Method.Equals("POST")
			   || !context.Request.HasFormContentType)
			{
				context.Response.StatusCode = 400;
				return context.Response.WriteAsync("Bad request.");
			}

			return GenerateToken(context);
		}

		private Task GenerateToken(HttpContext context)
		{
			var username = context.Request.Form["username"];
			var password = context.Request.Form["password"];

			var identity = accountService.CheckUserNameAndPass(username, password);
			if (!identity)
			{
				context.Response.StatusCode = 400;
				context.Response.WriteAsync("Invalid username or password.");
				return null;
			}

			var now = DateTime.UtcNow;

			// Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.
			// You can add other claims here, if you want:
			var claims = new Claim[]
			{
				new Claim(JwtRegisteredClaimNames.Sub, username),
				new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
				new Claim(JwtRegisteredClaimNames.Iat, now.ToLongDateString(), ClaimValueTypes.Integer64)
		};

			IdentityModelEventSource.ShowPII = true;
			// Create the JWT and write it to a string
			var jwt = new JwtSecurityToken(
				issuer: _options.Issuer,
				audience: _options.Audience,
				//claims: claims,
				notBefore: now,
				expires: now.Add(_options.Expiration),
				
				signingCredentials: _options.SigningCredentials);
			var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

			var response = new
			{
				access_token = encodedJwt,
				expires_in = (int)_options.Expiration.TotalSeconds,
				user_info = new UserDTO { Username = username }
			};

			// Serialize and return the response
			context.Response.ContentType = "application/json";
			context.Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
			return null;
		}

		private Task<ClaimsIdentity> GetIdentity(string username, string password)
		{
			var identity = accountService.CheckUserNameAndPass(username, password);
			// DON'T do this in production, obviously!
			if (identity)
			{
				return Task.FromResult(new ClaimsIdentity(new System.Security.Principal.GenericIdentity(username, "Token"), new Claim[] { }));
			}

			// Credentials are invalid, or account doesn't exist
			return Task.FromResult<ClaimsIdentity>(null);
		}
	}
}
