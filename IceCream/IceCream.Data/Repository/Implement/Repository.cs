﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace IceCream.Data.Repository.Implement
{
	public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
	{
		public Repository(IMyDbContext dbContext)
		{
			if (dbContext == null)
				throw new ArgumentNullException("Null DbContext");
			DbContext = dbContext;
			DbSet = DbContext.Set<TEntity>();
		}

		protected IMyDbContext DbContext { get; set; }

		protected DbSet<TEntity> DbSet { get; set; }

		public virtual IQueryable<TEntity> GetAll()
		{
			return DbSet;
		}

		public virtual TEntity GetById(int id)
		{
			return DbSet.Find(id);
		}

		public virtual void Add(TEntity entity)
		{
			DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
			if (dbEntityEntry.State != EntityState.Detached)
			{
				dbEntityEntry.State = EntityState.Added;
			}
			else
			{
				DbSet.Add(entity);
			}
		}

		public virtual void Update(TEntity entity)
		{
			DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
			if (dbEntityEntry.State == EntityState.Detached)
			{
				DbSet.Attach(entity);
			}
			dbEntityEntry.State = EntityState.Modified;
		}

		public virtual void Delete(TEntity entity)
		{
			DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
			if (dbEntityEntry.State != EntityState.Deleted)
			{
				dbEntityEntry.State = EntityState.Deleted;
			}
			else
			{
				DbSet.Attach(entity);
				DbSet.Remove(entity);
			}
		}

		public virtual void Delete(int id)
		{
			var entity = GetById(id);
			if (entity == null) return; // not found; assume already deleted.
			Delete(entity);
		}
	}
}
