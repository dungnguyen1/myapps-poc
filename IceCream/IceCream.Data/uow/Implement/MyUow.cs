﻿using IceCream.Data.Repository;
using IceCream.Data.Repository.Implement;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Data.uow.Implement
{
	public class MyUow : IMyUow, IDisposable
	{
		public IMyDbContext dbcontext { get; set; }

		private IRepository<Customer> _customerRepo { get; set; }

		public IRepository<Customer> CustomerRepository
		{
			get
			{
				if (_customerRepo == null)
				{
					_customerRepo = new Repository<Customer>(dbcontext);
				}

				return _customerRepo;
			}
		}

		private IRepository<Admin> _adminRepo { get; set; }

		public IRepository<Admin> AdminRepository
		{
			get
			{
				if (_adminRepo == null)
				{
					_adminRepo = new Repository<Admin>(dbcontext);
				}

				return _adminRepo;
			}
		}

		private IRepository<Recipe> _recipesRepo { get; set; }

		public IRepository<Recipe> RecipesRepository
		{
			get
			{
				if (_recipesRepo == null)
				{
					_recipesRepo = new Repository<Recipe>(dbcontext);
				}

				return _recipesRepo;
			}
		}

		private IRepository<OnlineOrder> _orderRepo { get; set; }

		public IRepository<OnlineOrder> OrdeRepository
		{
			get
			{
				if (_orderRepo == null)
				{
					_orderRepo = new Repository<OnlineOrder>(dbcontext);
				}

				return _orderRepo;
			}
		}

		public MyUow(IMyDbContext _myDbContext)
		{
			dbcontext = _myDbContext;
			CreateDbContext();
		}

		/// <summary>
		/// Save pending changes to the database
		/// </summary>
		public int Commit()
		{
			return dbcontext.SaveChanges();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (dbcontext != null)
				{
					dbcontext.Dispose();
				}
			}
		}

		public void CreateDbContext()
		{
			// Do NOT enable proxied entities, else serialization fails.
			//if false it will not get the associated certification and skills when we
			//get the applicants
			dbcontext.Configuration.ProxyCreationEnabled = false;

			// Load navigation properties explicitly (avoid serialization trouble)
			dbcontext.Configuration.LazyLoadingEnabled = false;

			// Because Web API will perform validation, we don't need/want EF to do so
			dbcontext.Configuration.ValidateOnSaveEnabled = false;

			//DbContext.Configuration.AutoDetectChangesEnabled = false;
			// We won't use this performance tweak because we don't need
			// the extra performance and, when autodetect is false,
			// we'd have to be careful. We're not being that careful.
		}
	}
}
