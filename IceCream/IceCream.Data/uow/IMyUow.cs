﻿using IceCream.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace IceCream.Data.uow
{
	public interface IMyUow : IDisposable
	{
		IMyDbContext dbcontext { get; set; }
		int Commit();
		void Dispose();
		IRepository<Customer> CustomerRepository { get; }
		IRepository<Admin> AdminRepository { get; }
		IRepository<Recipe> RecipesRepository { get; }
		IRepository<OnlineOrder> OrdeRepository { get; }
	}
}
